# Django and Docker commands

PROJECT_NAME = bettertogether
DJANGO_PROJECT_NAME = project
PROJECT_SRV = ${PROJECT_NAME}_SRV

.PHONY: all

.DEFAULT:
	@echo "Usage: "
	@make help

help: ## Show this help.
	# From https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
build: ## Build / rebuild the project.
	@docker-compose build
dbshell: ## Access Django DB shell inside project container.
	@docker-compose run ${PROJECT_NAME} python3 ${DJANGO_PROJECT_NAME}/manage.py dbshell
djangoshell: ## Access Django shell_plus inside project container.
	@docker-compose run ${PROJECT_NAME} python3 ${DJANGO_PROJECT_NAME}/manage.py shell_plus
lint:  ## Run linting on Project.
	@scripts/code-linter.sh
loaddata: ## Initialize project - create 'admin/superuser' superuser and load initial sample data. Should be run only once at beginning.
	@docker-compose run ${PROJECT_NAME} python3 ${DJANGO_PROJECT_NAME}/manage.py import_initial_data users
	@docker-compose run ${PROJECT_NAME} python3 ${DJANGO_PROJECT_NAME}/manage.py import_initial_data venues
logs: ## Show project container logs in "follow" mode.
	docker logs -f ${PROJECT_SRV}
migrations: ##  Runs 'manage.py makemigrations'. Note: it will generate standar migration names. If we want to customize 'name', we will have ro run the command directly to supply the option.
	@docker-compose run ${PROJECT_NAME} python3 ${DJANGO_PROJECT_NAME}/manage.py makemigrations
osshell:  ## Run a OS shell inside project container.
	@docker-compose run ${PROJECT_NAME} bash
pdb: ## Attach to ${PROJECT_SRV} so can interact with pdb
	@docker attach ${PROJECT_SRV}
restart:  stop startbg ## Restart Project.
start: ## Start project running in a non-detached mode.
	@docker-compose up
startclient: ## Start a simple HTML Client Server to consume Projec endpoits. Default port is 8003.
	@clients/html/start_html_client.sh
startbg: ## Start project running in detached mode - background.
	@docker-compose up -d
stop: ## Stop the running project.
	@docker-compose stop
tests: ## Run unit tests in project. Alternatively tests can be run in interactive mode using 'make osshell'.
	@docker-compose run ${PROJECT_NAME} pytest
