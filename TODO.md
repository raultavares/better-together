### Bettertogether project to-do list
#### Things to add / beautify, after delivering the first MVP

- Admin - make a better presentation of M2M relationships
- DRF API - Build the proper functionalities
- Data import - Data pre-validation / sanitization.
- Make front-end presentation consistent
- Display food and drink for Venues
- Tests. TDD not shinning...
- Validate results

----------
  