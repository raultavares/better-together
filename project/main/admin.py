# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import (
    Drink,
    Food,
    User,
    Venue,
)

admin.site.register(
    [
        Drink,
        Food,
        User,
        Venue,
    ],
)
