import pytest
from main.models import(
    Drink,
    Food,
    User,
    Venue,
)


# pytestmark = pytest.mark.django_db

def test_venue_have_drinks_and_food(db):
    """ Verify that we can create Venue and related foof / drink """
    venue = Venue.objects.create(name='Venue name')
    drink = Drink.objects.create(name='Water')
    food = Food.objects.create(name='Pasta')
    #
    # Objects have to be created before can be related
    venue.drinks.add(drink)
    venue.food.add(food)

    assert Venue.objects.all().first().drinks.all().first().name == Drink.objects.all().first().name
    assert Venue.objects.all().first().food.all().first().name == Food.objects.all().first().name
