# -*- coding: utf-8 -*-
from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'listdata', views.PersonViewSet)

urlpatterns = [
    path('', views.Home.as_view()),
    path('venues/', views.Venues.as_view()),
    path('api/select_venues/', views.SelectVenues.as_view()),
    path(r'api/', include(router.urls)),
]
