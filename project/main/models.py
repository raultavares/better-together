# -*- coding: utf-8 -*-
from django.db import models

# Default field lengths
# 'Description' will be a TextField / TextArea
TEXT_FIELD_LENGTH = 100


class Drink(models.Model):
    """ Drink types"""
    name = models.CharField(max_length=TEXT_FIELD_LENGTH)
    help_text = "Drink type's name."

    def __str__(self):
        return f"{self.name}"


class Food(models.Model):
    """ Food types"""
    name = models.CharField(max_length=TEXT_FIELD_LENGTH)
    help_text = "Food type's name."

    def __str__(self):
        return f"{self.name}"


class User(models.Model):
    """ User's tastes """
    name = models.CharField(max_length=TEXT_FIELD_LENGTH)
    wont_eat = models.ManyToManyField(Food, blank=True)
    drinks = models.ManyToManyField(Drink, blank=True)
    notes = models.TextField(blank=True, null=True)
    help_text = "User name."

    def __str__(self):
        return f"User: {self.pk} - {self.name}"


class Venue(models.Model):
    """ Venue menu choices """
    name = models.CharField(max_length=TEXT_FIELD_LENGTH)
    food = models.ManyToManyField(Food, blank=True)
    drinks = models.ManyToManyField(Drink, blank=True)
    notes = models.TextField(blank=True, null=True)
    help_text = "Venue Name."

    def __str__(self):
        return f"Venue: {self.pk} - {self.name}"
