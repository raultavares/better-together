# -*- coding: utf-8 -*-
"""
Note: in this import, there is no data integrity validation nor reporting.
It is due being a test project and time constraints.
In a real world project we would have to do data integrity validation / sanitization,
pre-validation of dataset content before import, and reporting on the results, success or not.
"""
import json
import os
import sys

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from main.models import (
    Food,
    Drink,
    User as GoingOutPal,
    Venue
)


class Command(BaseCommand):
    help = "Loads initial application data, including the creation of a Superuser. " \
           "Argument: 'users' or 'venues' on 'data_upload. folder."

    upload_dir = 'data_upload'
    data_file_suffix = 'json'

    def create_superuser(self):
        """ Check if superuser is already created, and create it if not. """
        try:
            User.objects.get(username='admin')
        except User.DoesNotExist:
            new_user = User.objects.create(
                username='admin',
                email='abc@def.com',
                is_superuser=True,
                is_staff=True
            )
            new_user.set_password('superuser')
            new_user.save()

    def add_user_data(self, data_items):
        """ Load data to main.User. """

        user, created = GoingOutPal.objects.get_or_create(name=data_items['name'])

        if data_items['drinks']:
            for drink in data_items['drinks']:
                drk, d_created = Drink.objects.get_or_create(name=drink)
                user.drinks.add(drk)

        if data_items['wont_eat']:
            for food in data_items['wont_eat']:
                fdd, f_created = Food.objects.get_or_create(name=food)
                user.wont_eat.add(fdd)

    def add_venue_data(self, data_items):
        """ Load data to models. """
        venue, created = Venue.objects.get_or_create(name=data_items['name'])

        if data_items['drinks']:
            for drink in data_items['drinks']:
                drk, d_created = Drink.objects.get_or_create(name=drink)
                venue.drinks.add(drk)

        if data_items['food']:
            for food in data_items['food']:
                fdd, f_created = Food.objects.get_or_create(name=food)
                venue.food.add(fdd)

    def add_arguments(self, parser):
        parser.add_argument('file', type=str, help="'users' or 'venues' on 'data_upload. folder.")

    def handle(self, *args, **kwargs):
        # Check if superuser is already created, and create it if not.
        self.create_superuser()

        data_upload_folder = os.path.join(os.getcwd(), self.upload_dir)
        data_file = kwargs['file']
        data_upload_file = os.path.join(data_upload_folder, f"{data_file}.{self.data_file_suffix}")

        if not os.path.isfile(data_upload_file):
            sys.exit(f"ERROR! {data_upload_file} cannot be found!")

        if data_file == 'users':
            add_data = self.add_user_data
        elif data_file == 'venues':
            add_data = self.add_venue_data

        print(f"Loading '{data_upload_file}'")

        with open(data_upload_file) as data_file:
            try:
                data_load = json.load(data_file)
            except Exception as e:
                sys.exit(f"Error in importing: {e}")

            for idx, data_item in enumerate(data_load):
                print(idx, end=' ')
                add_data(data_item)
