# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import six
from django.views import View

from rest_framework import viewsets

from .models import (
    User,
    Venue,
)
from .serializers import (
    UserSerializer,
)
from .utils import (
    get_venues_for_group,
)


class Home(View):
    """ Template View """
    template_name = 'main/home.html'
    context = {
        'page_title': 'Home Page',
        'data': {
            'name': "Home Page",
        }
    }

    def get(self, request):
        user_is_logged = request.user.is_authenticated

        self.context['user_is_logged'] = user_is_logged

        content = User.objects.all()

        if user_is_logged:
            # Stuff exclusive to people we already know
            pass

        self.context['content'] = content

        return render(request, self.template_name, self.context)


class Venues(View):
    """ Template View """
    template_name = 'main/venues.html'
    context = {
        'page_title': 'Venues List',
        'data': {
            'name': "Venues List",
        }
    }

    def get(self, request):
        user_is_logged = request.user.is_authenticated

        self.context['user_is_logged'] = user_is_logged

        content = Venue.objects.all()

        if user_is_logged:
            # Stuff exclusive to people we already know
            pass

        self.context['content'] = content

        return render(request, self.template_name, self.context)


class SelectVenues(View):
    """ Receive a list od users, and return the appropriated venues based on their preferences. """
    def get(self, request, *args, **kwargs):
        users_list = [int(x) for x in dict(six.iterlists(request.GET))['users[]']]
        selected_venues, excluded_venues = get_venues_for_group(users_list)

        result = {
            'accepted': selected_venues,
            'excluded': excluded_venues,
        }
        return JsonResponse(result)


class PersonViewSet(viewsets.ModelViewSet):
    """ DRF View """
    queryset = User.objects.all().order_by('name')
    serializer_class = UserSerializer
