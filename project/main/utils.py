# -*- coding: utf-8 -*-
""" Utility functions """
from django.core import serializers

from .models import (
    User,
    Venue,
)


def get_venues_for_group(group: list) -> tuple:
    """
    Retrieves and returns a list of accepted and rejected Venues for the selected Group.
    :param group: List od ID's of Users forming the Group
    :return: Tuple with a list of selected and excluded Venues
    :rtype: Tuple
    """
    selected_users = User.objects.filter(pk__in=group)

    # -------------
    # Messy implementation for MVP. Needs optimization to avoid so many hits to DB.
    # Too much logic in View. Move functionalities to Model methods
    # Need names for food and drinks in Venues
    # Needed tests for validation.
    # --------------
    drinks = set([y.pk for x in selected_users for y in x.drinks.all()])
    wont_eat = set([y.pk for x in selected_users for y in x.wont_eat.all()])

    selected_venues = serializers.serialize(format='json',
                                            queryset=Venue
                                            .objects.filter(drinks__in=drinks)
                                            .exclude(food__in=wont_eat).distinct())

    excluded_venues = serializers.serialize(format='json',
                                            queryset=Venue.objects
                                            .filter(food__in=wont_eat)
                                            .distinct())

    return selected_venues, excluded_venues
