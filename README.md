![TimeOut](project/main/static/main/img/timeout_logo.jpg)
## TimeOut - Better together - Going out     
#### Live at: [http://timeout.uk.to/](http://timeout.uk.to/)   

-------
### Description:
- Test project for TimeOut
- Going out is better together.   
However, pals have different choices and preferences for eating and drinking.  
This small project aims to pair venues with personal preferences of group members to ease the choice of the place to go.  

    However, after all, what is important is being together and having fun! 

This project was built based on my previous 'pet project' [Django Headless Blueprint](https://bitbucket.org/raultavares/django-headless-blueprint), as an 'application accelerator'.
   
### Project structure:
- Docker / docker-compose
- Gunicorn
- dotenv for control dev/prod environment
- Separated dev / prod settings, driven by .env
- SECRET KEY really secret
- Make to make our life easy
- Utility shell scripts
- iPython / shell_plus
- Already a basic 'main' app in place
- Some loaded sample data
- Grapelli to make Admin even better
- DRF APIs
- cors-headers to allow local access to API
- Test by PyTest 
- Basic HTML / jQuery client
- '_Bootstrap_' to beautify our pages 

### Setting up
- Clone the project locally.
- Create a `.env` file from `dot_env_project`.
- Run `make build` to initialize the standard `project` project.  
- Run `make loaddata` to load initial data and create a `admin/superuser` superuser.  
- Run `make start` to see something happening
    - Alternatively, run `make startbg` to start the project in background - detached.
- Run `make help` to see what more it can do for you.  
- Alternatively we can run `docker-compose` commands directly:
    - `docker-compose up --build -d` 
    - `docker-compose up -d` 
    - `docker-compose stop` 
- To run Django `manage.py` commands:
    - `docker-compose run bettertogether python3 project/manage.py {command}` 
- To access shell_plus and ipython - same as `make djangoshell`:
    - `docker-compose run bettertogether python3 project/manage.py shell_plus --ipython` 
 
### Running:
 - `make start` and you will get site available in `localhost:8000`.  
    - This will start `docker-compose` in `attached` mode.
    - To stop: 
        - `ctrl-C` 
        - or `make stop` in other window.
        - or ` ctrl-z` in same window and `make stop`.
 - `make startbg` - start project in background - detach.
- `make startclient` - starts a minimalistic HTML API client on standard port `8002`. 
 
### Accessing:
 - `localhost:8000` - Standard Django 'view/template' page.  
 - After running `make start_client` we will be able to access at `localhost:8002` - a minimal HTML / jQuery Client to consume Django Project API endpoints.

### Using:
- Initial project data will be loaded by `make loaddata`. 
- It will also allow access to `/admin` with `admin/superuser`.
- In front-end, select the group members that will be going out to find their common preferences in drinks and food, and see the selection of the best venues that better suits them all.
- Run `make tests` to see the results of unit tests in place.

-----
